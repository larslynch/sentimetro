﻿using System;
using Sentimetro.Comun.Enums;
using Sentimetro.Entities.Core;

namespace Sentimetro.Entities
{
    public class Valoracion : BaseEntity
    {
        public eNivelFelicidad NivelFelicidad { get; set; }

        public eNivelCarga NivelCarga { get; set; }

        public bool Test = true;
        public DateTime TimeStamp;
    }
}
