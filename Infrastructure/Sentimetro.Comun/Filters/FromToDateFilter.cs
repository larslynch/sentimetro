﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sentimetro.Comun.Filters
{
    public class FromToDateFilter
    {
        public FromToDateFilter()
        {

        }
        public DateTime? From;
        public DateTime? To;
    }
}
