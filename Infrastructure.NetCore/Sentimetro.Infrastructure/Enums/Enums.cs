﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sentimetro.Infrastructure.Enums
{
    public enum eHapiness
    {
        Sad = 0,
        Normal = 1,
        Angry = 2
    }

    public enum eWorkload
    {
        Low = 0,
        Normal = 1,
        Many = 2
    }
}
