﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sentimetro.Infrastructure.Enums;

namespace Sentimetro.Entities
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class Assessment
    {
        #region Ctor
        public Assessment()
        {
        }
        #endregion Ctor

        public Guid Id { get; set; }
        public bool Test { get; set; }
        public eHapiness HapinessLevel { get; set; }
        public eWorkload Workload { get; set; }
        public DateTime Timestamp { get; set; }


    }
}
