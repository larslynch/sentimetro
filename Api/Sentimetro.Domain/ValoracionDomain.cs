﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sentimetro.Comun.Filters;
using Sentimetro.Entities;
using Sentimetro.Repository;

namespace Sentimetro.Domain
{
    public class ValoracionDomain
    {
        private readonly ValoracionRepository repository;

        public ValoracionDomain()
        {
            this.repository = new ValoracionRepository();
        }

        public async Task<IEnumerable<Valoracion>> GetAsync(FromToDateFilter filter, bool isTest)
        {
            return await repository.GetAsync(filter, isTest);
        }

        public async Task<List<Valoracion>> GetAllAsync()
        {
            var result = await repository.GetAllAsync();
            return result.ToList();
        }
        public async Task SaveAsync(Valoracion valoracion)
        {
            await repository.SaveAsync(valoracion);
        }
    }
}
