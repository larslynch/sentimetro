﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;

namespace Sentimetro.Context
{
    public class Context
    {
        public IMongoClient _client;
        public IMongoDatabase _database;
        public MongoClientSettings _settings;
        private static string dbName = "sentimetro";
        private static string dbUser = "sentimetro";
        private static string dbPassword = "sentimetroTKT";
        private static string dbServerName = "ds064188.mlab.com:64188";
        private static int dbServerPort = 64188;

        public Context()
        {
            dbName = ConfigurationManager.AppSettings["dbName"];
            dbUser = ConfigurationManager.AppSettings["dbUser"];
            dbPassword = ConfigurationManager.AppSettings["dbPassword"];
            dbServerName = ConfigurationManager.AppSettings["dbServerName"];
            dbServerPort = int.Parse(ConfigurationManager.AppSettings["dbServerPort"]);


            _settings = new MongoClientSettings
            {

                Credentials = new MongoCredential[]
                {
                    MongoCredential.CreateCredential(dbName, dbUser, dbPassword)
                },
                Server = new MongoServerAddress(dbServerName, dbServerPort),

            };

            _client = new MongoClient(_settings);
            _database = _client.GetDatabase(dbName);
        }

    }
}
