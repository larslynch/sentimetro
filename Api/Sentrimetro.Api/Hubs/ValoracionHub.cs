﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Sentimetro.Entities;
using Sentimetro.Comun.Enums;

namespace Sentrimetro.Api.Hubs
{
    public class ValoracionHub : Hub
    {
        public override System.Threading.Tasks.Task OnConnected()
        {
            return base.OnConnected();
        }

        public void Hello()
        {
            this.Send(new Valoracion()
            {
                NivelCarga = eNivelCarga.Normal,
                NivelFelicidad = eNivelFelicidad.Normal
            });
        }

        public void Send(Valoracion valoracion)
        {
            Clients.All.addValoracion(valoracion);
        }
    }
}