﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sentimetro.IRepository.Core
{
    public interface IRepositoryCore<T>
    {
        Task<IEnumerable<T>> GetAll();
    }
}
