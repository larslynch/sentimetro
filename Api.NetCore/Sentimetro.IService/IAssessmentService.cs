﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sentimetro.Entities;
using Sentimetro.IService.Core;

namespace Sentimetro.IService
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public interface IAssessmentService : ICoreService
    {

        Task<List<Assessment>> GetAll();
    }
}
