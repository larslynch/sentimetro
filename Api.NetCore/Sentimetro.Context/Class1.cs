﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;
using Microsoft.Extensions.OptionsModel;

namespace Sentimetro.Context
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class Context
    {
        public IMongoClient _client;
        public IMongoDatabase _database;
        public MongoClientSettings _settings;
        private static string dbName = "sentimetro";
        private static string dbUser = "sentimetro";
        private static string dbPassword = "sentimetroTKT";
        private static string dbServerName = "ds064188.mlab.com:64188";
        private static int dbServerPort = 64188;

        public Context(IOptions<Settings> settings)
        {
            //dbName = ConfigurationManager.AppSettings["dbName"];
            //dbUser = ConfigurationManager.AppSettings["dbUser"];
            //dbPassword = ConfigurationManager.AppSettings["dbPassword"];
            //dbServerName = ConfigurationManager.AppSettings["dbServerName"];
            //dbServerPort = int.Parse(ConfigurationManager.AppSettings["dbServerPort"]);


            _settings = new MongoClientSettings
            {

                Credentials = new MongoCredential[]
                {
                    MongoCredential.CreateCredential(dbName, dbUser, dbPassword)
                },
                Server = new MongoServerAddress(dbServerName, dbServerPort),

            };

            _client = new MongoClient(_settings);
            _database = _client.GetDatabase(dbName);
        }

    }
}
