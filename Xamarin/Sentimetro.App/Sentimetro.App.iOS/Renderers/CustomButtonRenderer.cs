﻿using Sentimetro.App.iOS.Renderers;
using Sentimetro.App.Views.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Sentimetro.App.iOS.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control == null) return;
            Control.BackgroundColor = UIColor.Blue;
            Control.TintColor = UIColor.White;
        }
    }
}
