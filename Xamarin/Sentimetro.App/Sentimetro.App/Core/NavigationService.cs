﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.ServiceLocation;
using Xamarin.Forms;

namespace Sentimetro.App.Core
{
    public class NavigationService : INavigationService
    {
        private readonly Dictionary<string, Tuple<Type, Type>> _pagesByKey = new Dictionary<string, Tuple<Type, Type>>();
        private readonly IServiceLocator _locator;
        private readonly NavigationPage _navigation;

        public NavigationService(IServiceLocator locator, NavigationPage navigation)
        {
            _locator = locator;
            _navigation = navigation;
        }

        public void Configure<T, TV>(string identifier = null) where T:class where TV:class
        {
            lock (_pagesByKey)
            {
                var key = string.IsNullOrEmpty(identifier) ? typeof(T).ToString() : identifier;
                if (_pagesByKey.ContainsKey(key))
                {
                    _pagesByKey[key] = new Tuple<Type, Type>(typeof(T), typeof(TV));
                }
                else
                {
                    _pagesByKey.Add(key, new Tuple<Type, Type>(typeof(T), typeof(TV)));
                }
            }
        }

        public void GoBack()
        {
            _navigation.PopAsync();
        }

        public void NavigateTo<T>() where T:class
        {
            NavigateTo<T>(null);
        }


        public void NavigateTo<T>( object parameter, string identifier = null) where T : class
        {
            var page = GetPage(typeof(T), parameter, identifier);
            _navigation.PushAsync(page);
        }

        private Page GetPage(Type viewModelType, object parameter, string identifier)
        {
            lock (_pagesByKey)
            {
                var key = string.IsNullOrEmpty(identifier) ? viewModelType.ToString() : identifier;
                if (!_pagesByKey.ContainsKey(key))
                    throw new ArgumentException(
                        $"No such page: {key}. Did you forget to call NavigationService.Configure?",
                        nameof(key));

                var typePair = _pagesByKey[key];
                ConstructorInfo constructor;
                object[] parameters;

                if (parameter == null)
                {
                    constructor = typePair.Item2.GetTypeInfo()
                        .DeclaredConstructors
                        .FirstOrDefault(c => !c.GetParameters().Any());

                    parameters = new object[] {};
                }
                else
                {
                    constructor = typePair.Item2.GetTypeInfo()
                        .DeclaredConstructors
                        .FirstOrDefault(
                            c =>
                            {
                                var p = c.GetParameters();
                                return p.Length == 1
                                       && p[0].ParameterType == parameter.GetType();
                            });

                    parameters = new[] {parameter};
                }

                if (constructor == null)
                {
                    throw new InvalidOperationException(
                        "No suitable constructor found for page " + key);
                }

                var page = (Page) constructor.Invoke(parameters);
                page.BindingContext = GetViewModel(typePair.Item1);
                return page;
            }
        }

        private object GetViewModel(Type viewModelType)
        {
            var vm = _locator.GetInstance(viewModelType);
            return vm;
        }
    }
}
