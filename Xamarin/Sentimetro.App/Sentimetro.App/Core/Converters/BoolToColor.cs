﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Sentimetro.App.Core.Converters
{
    public class BoolToColor :IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? Color.Yellow : Color.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
