﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Refit;
using Sentimetro.App.Core;
using Sentimetro.App.Services;
using Sentimetro.Comun.Enums;
using Sentimetro.Entities;

namespace Sentimetro.App.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private CustomButtonViewModel _selectedMood;
        private CustomButtonViewModel _selectedWorkload;
        private bool _isSending;

        private ObservableCollection<CustomButtonViewModel> _moods;
        public ObservableCollection<CustomButtonViewModel> Moods
        {
            get { return _moods; }
            set
            {
                if (_moods == value) return;
                _moods = value;
                RaisePropertyChanged(nameof(Moods));
            }
        }

        private ObservableCollection<CustomButtonViewModel> _workLoad;
        public ObservableCollection<CustomButtonViewModel> Workload
        {
            get { return _workLoad; }
            set
            {
                if (_workLoad == value) return;
                _workLoad = value;
                RaisePropertyChanged(nameof(Workload));
            }
        }

        private ICommand _clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new RelayCommand(async() =>
                {
                    var api = RestService.For<IValoracionService>("http://sentimetrotkt.azurewebsites.net/");
                    var v = new Valoracion
                    {
                        NivelCarga = (eNivelCarga) _selectedWorkload.OriginalValue,
                        NivelFelicidad = (eNivelFelicidad) _selectedMood.OriginalValue,
                        Test = true,
                        TimeStamp = DateTime.UtcNow
                    };
                    try
                    {
                        _isSending = true;
                        RefreshCommandStatus();
                        var res = await api.Insert(v);
                        if (res.StatusCode == HttpStatusCode.OK)
                        {
                            SetButtonsToFalse(Moods);
                            SetButtonsToFalse(Workload);
                            _selectedMood = null;
                            _selectedWorkload = null;
                            //show some message to the user
                            Messenger.Default.Send(new NotificationMessage<string>("Information successfully sent to the server","OK"));
                        }
                        else
                        {
                            //something went wrong, warn the user
                            Messenger.Default.Send(new NotificationMessage<string>("Something went wrong, please try again.", "KO"));
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                    _isSending = false;
                    RefreshCommandStatus();


                }, () => _selectedMood != null && _selectedWorkload != null && _isSending == false));
            }
        }

        private ICommand _moodCommand;
        public ICommand MoodCommand
        {
            get
            {
                return _moodCommand ?? (_moodCommand = new RelayCommand<CustomButtonViewModel>(x =>
                {
                    _selectedMood = x;
                    SelectCustomButton(x, Moods);
                }));

            }
        }

        private ICommand _workloadCommand;
        public ICommand WorkloadCommand
        {
            get
            {
                return _workloadCommand ?? (_workloadCommand = new RelayCommand<CustomButtonViewModel>(x =>
                {
                    _selectedWorkload = x;
                    SelectCustomButton(x, Workload);
                }));
            }
        }

        private void SelectCustomButton(CustomButtonViewModel x, IEnumerable<CustomButtonViewModel> list )
        {
            SetButtonsToFalse(list);
            x.IsSelected = true;
            RefreshCommandStatus();
        }

        private void RefreshCommandStatus()
        {
            ((RelayCommand) ClickCommand).RaiseCanExecuteChanged();
        }

        private static void SetButtonsToFalse(IEnumerable<CustomButtonViewModel> list)
        {
            foreach (var customButtonViewModel in list)
            {
                customButtonViewModel.IsSelected = false;
            }
        }

        public MainViewModel(INavigationService navigationService) : base(navigationService)
        {
            //set collections
            Moods = GetButtonCollection(typeof (eNivelFelicidad));
            Workload = GetButtonCollection(typeof (eNivelCarga));
        }

        private ObservableCollection<CustomButtonViewModel> GetButtonCollection(Type enumType)
        {   var res = new ObservableCollection<CustomButtonViewModel>();
            foreach (var value in Enum.GetValues(enumType))
            {
                var cb = 
                    new CustomButtonViewModel(NavigationService)
                    {
                        IsSelected = false,
                        Value = (int) value,
                        Text = value.ToString(),
                        OriginalValue = value
                    };
                res.Add(cb);
            }
            return res;
        } 
    }
}
