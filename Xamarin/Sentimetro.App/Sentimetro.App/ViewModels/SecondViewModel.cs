﻿using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Sentimetro.App.Core;

namespace Sentimetro.App.ViewModels
{
    public class SecondViewModel: ViewModelBase
    {
        public string MainText { get; set; }

        private ICommand _clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new RelayCommand(() => NavigationService.GoBack()));
            }
        }

        public SecondViewModel(INavigationService navigationService) : base(navigationService)
        {
            MainText = "Hello World From Page 2!";
        }
    }
}
