﻿using System;
using Sentimetro.App.Core;

namespace Sentimetro.App.ViewModels
{
    public class CustomButtonViewModel : ViewModelBase
    {
        public int Value { get; set; }
        public string Text { get; set; }

        public object OriginalValue { get; set; }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                RaisePropertyChanged(nameof(IsSelected));
            }
        }

        public CustomButtonViewModel(INavigationService navigationService) : base(navigationService)
        {
        }
    }
}
