﻿

using Sentimetro.App.Core;

namespace Sentimetro.App.ViewModels
{
    public class ViewModelBase :GalaSoft.MvvmLight.ViewModelBase
    {
        public INavigationService NavigationService { get; private set; }

        public ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }
    }
}
