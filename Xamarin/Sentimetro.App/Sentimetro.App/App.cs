﻿using Sentimetro.App.Core;
using Xamarin.Forms;

namespace Sentimetro.App
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            var conf = new AppConfiguration();
            MainPage = conf.FirstPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
