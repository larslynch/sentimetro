﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Refit;
using Sentimetro.Entities;

namespace Sentimetro.App.Services
{
    public interface IValoracionService
    {
        [Get("/api/valoracion")]
        Task<IEnumerable<Valoracion>> Get();

        [Get("/api/valoracion?from={from}")]
        Task<IEnumerable<Valoracion>> GetFrom(DateTime from);

        [Post("/api/valoracion")]
        Task<HttpResponseMessage> Insert( [Body]Valoracion request);
    }
}
